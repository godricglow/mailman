# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
# This file is part of GNU Mailman.
#
# GNU Mailman is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# GNU Mailman is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# GNU Mailman.  If not, see <http://www.gnu.org/licenses/>.

"""Model for threads in dynamic sub-lists."""

__all__ = [
    'Thread',
    'ThreadManager',
    ]


import os
import re

from email.utils import formatdate

from mailman.config import config
from mailman.database.model import Model
from mailman.database.types import Enum
from mailman.database.transaction import dbconnection
from mailman.interfaces.dlist import ( DlistPreference, IOverride, IOverrideManager, IThread, IThreadManager)
from mailman.model.preferences import Preferences
from mailman.runners.lmtp import split_recipient
from mailman.utilities.string import expand
from mailman.utilities.email import add_thread_hash
from sqlalchemy import (
    Boolean, Column, DateTime, Float, ForeignKey, Integer, Interval,
    LargeBinary, PickleType, Table, Unicode)
from sqlalchemy.event import listen
from sqlalchemy.orm import relationship
from zope.component import getUtility
from zope.interface import implementer

association_table = Table('association', Model.metadata,
    Column('override_id', Integer, ForeignKey('override.id')),
    Column('thread_id', Integer, ForeignKey('thread.id'))
)

@implementer(IThread)
class Thread(Model):
    """See `IThread`."""

    __tablename__ = 'thread'
    
    id = Column(Integer, primary_key=True)
    thread_id = Column(Unicode)
    thread = relationship("Override",
                    secondary=association_table,
                    backref="Thread")
    member_id = Column(Unicode, ForeignKey('member.id'))

    def __init__(self, thread_id):
        super(Thread, self).__init__()
        self.thread_id = thread_id

@implementer(IOverride)
class Override(Model):
    """See `IOverride`."""

    __tablename__ = 'override'
    id = Column(Integer, primary_key=True)
    member_id = Column(Unicode, ForeignKey('member.id'))
    preference = Column(Enum(DlistPreference))

    def __init__(self, member_id, thread_id, preference):
        super(Override, self).__init__()
        self.member_id = member_id
        self.thread_id = thread_id
        self.preference = preference

# Add everything in adapter

@implementer(IThreadManager)
class ThreadManager():
    """ Thread manager."""

    @dbconnection
    def create_thread(self, store, thread_id):
        """See `IThreadManager`."""
        thread = Thread(thread_id)
        store.add(thread)

    def new_thread(self, mlist, msg):
        add_thread_hash(msg)
        self.create_thread(msg['Thread-ID'])
        # Here we add the 'Reply-To:' for the message
        msg['Reply-To'] =  '%s+%s@%s' % (mlist.list_name,
                                   msg['Thread-ID'],
                                   mlist.web_host)

    @dbconnection
    def continue_thread(self, store, mlist, msg):
        # Here we have two cases, since the default setting is all posts
        # if a member wish to 'leave' a thread we override preference to
        # first post for that thread.

        #First we verify if the thread id exists in database
        thread_id = msg['Reply-To'].split('+',1)[1].split('-',1)[0]
        query = store.query(Thread).filter_by(thread_id=thread_id).all()
        if not query:
            print('Thread not found')
            #Add relevant Error
        else:
            pass

        preference = split_recipient(msg['Reply-To'])[1]
        member = mlist.members.get_member(msg.sender)
        override_manager = getUtility(IOverrideManager)
        if preference == 'allposts':
            override_manager.override(member.member_id, thread_id, DlistPreference.all_posts)

        # If the user sets first post only as default preference and wishes
        # to join a prticular thread then the preference is changed to all
        # post for that thread.

        if preference == 'firstpost':
            override_manager.override(member.member_id, thread_id, DlistPreference.first_posts)

        

@implementer(IOverrideManager)
class OverrideManager():
     """ Override manager."""

     @dbconnection
     def override_in(self, store, member_id, thread_id, preference):
        """See `IOverrideManager`."""
        override = Override(member_id, thread_id, preference)
        store.add(override)

     def override(self, member_id, thread_id, preference):
        self.member_id = member_id
        self.thread_id = thread_id
        self.preference = preference
        self.override_in(member_id, thread_id, preference)