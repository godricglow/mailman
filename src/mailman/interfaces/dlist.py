# Copyright (C) 2007-2015 by the Free Software Foundation, Inc.
#
# This file is part of GNU Mailman.
#
# GNU Mailman is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# GNU Mailman is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# GNU Mailman.  If not, see <http://www.gnu.org/licenses/>.

"""Interface for threads in dynamic sub-list."""

__all__ = [
    'IThread',
    'DlistPreference',
    ]


from enum import Enum
from mailman.interfaces.member import MemberRole
from zope.interface import Interface, Attribute


class IThread(Interface):
    """ Thread info for a mialing list. """

    thread_id = Attribute("""
        The thread id string.
        """)
    member_id = Attribute("""
        The member id string.
        """)

class IOverride(Interface):
    """ Thread info for a mialing list. """

    preference = Attribute("""
        The preference to be overriden.
        """)
    member_id = Attribute("""
        The member id string.
        """)

class IThreadManager(Interface):
    
    def create_thread(thread_id):
        """Creates the thread id.

        TODO: Documentation!
        """


    def new_thread(mlist, msg):
        """ Starts a new thread, including creating and enqueueing the
        initial messages. 

        :param mlist: The mailing list object.
        :param msg: The message object.
   
        """

    def continue_thread(mlist, msg):
        """ Add an existing message to a thread. 

        :param mlist: The mailing list object.
        :param msg: The message object.
        :param msgdata: The message metadata.
        :param thread_id The thread id for the message.

        """

class IOverrideManager(Interface):

    def override_in(member_id, thread_id, preference):
        """ Override an existing preference.

        :param  member_id: The member id.
        :param thread_id: The thread Id.
        :param preference: The preference.
        """

    def override(member_id, thread_id, preference):
        """ Override an existing preference.

        :param  member_id: The member id.
        :param thread_id: The thread Id.
        :param preference: The preference.
        """

class DlistPreference(Enum):
        
        # Recieve all posts in all thread and then explicitly unsubscribe to
        # a particular thread
        all_posts = 1
        # Receive first posts from each thread and explicitly subscribe to the
        # conversation
        first_posts = 2