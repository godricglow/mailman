# Copyright (C) 2007-2015 by the Free Software Foundation, Inc.
#
# This file is part of GNU Mailman.
#
# GNU Mailman is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# GNU Mailman is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# GNU Mailman.  If not, see <http://www.gnu.org/licenses/>.

"""The Dynamic sublist rule."""

__all__ = [
    'Dlist',
    ]


import re

from email.utils import getaddresses
from mailman.core.i18n import _
from mailman.interfaces.rules import IRule
from zope.interface import implementer


@implementer(IRule)
class Dlist:
	""" The Dynamic sublist rule. """
	
	name = 'dlist'
	description = _('Catch messages that are Dlist enabled')
	record = True

	def check(self, mlist, msg, msgdata):
		"""See `IRule`."""
		# If the mailing list is not dlist enabled, message is checked
		# for normal rules.
		# Subaddress must contain keyword "new" starting by "+" delimiter
		# Case 1: +new@domain
		if  mlist.dlist_enabled == True and msg['To'].split('+',1)[1].split('@',1)[0] == "new":
			return True
		# Case 2: +new-request@domain
		elif  mlist.dlist_enabled == True and msg['To'].split('+',1)[1].split('-',1)[0] == "new":
			return True
		else:
			return False	
			# Add relevant Error
		